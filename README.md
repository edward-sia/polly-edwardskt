# AWS React App - Serverless Speech Synthesiser

## Project in a Nutshelf

The project allows authenticated user to turn text into speech and replay audio. It is written in ReactJS and utilizing a range of AWS services with a fully automated CI/CD environment.
This project is purely designed to run without any business context however leveraging the capability of AWS Polly and AWS on-demand serverless architecture.

## Architectures Overview

### - AWS Architecture

![Architecture](media/AWS-architecture.png)

AWS Services used:

* AWS Polly
* AWS Cognito User Pools
* AWS API Gateway
* AWS Lambda
* AWS SNS
* AWS DynamoDB
* AWS S3
* AWS Mobile Hub

---

### - DevOps Architecture

![Architecture](media/infra-architecture.png)

Infrastructures used:

* GitLab
* GitLab Pipeline
* AWS CLI
* AWS S3
* AWS Mobile CLI

---

### Other services/dependencies

* Cloudflare CDN
* Material-UI

---

## Application walkthrough with screenshots

1. User authenticates against AWS Cognito User pool / Or choose to sign up.

![walkthrough](media/readme/1.png)

2. The entry point to our backend API gateway - POST, GET, DELETE.
3. The user creates a new text-to-speech request (POST).

![walkthrough](media/readme/2.png)

4. A lambda function is triggered by creating a new record entry in AWS DynamoDB.
5. Secondly, the lambda publishes a new notification via AWS SNS.
6. Another lambda function designed to listen on particular notification topic picks up a new post request.
7. The lambda function synthesizes text into speech and results in a new mp3 object.
8. The mp3 audio is saved in an S3 audio bucket with publicly accessible permission.
9. The URL of mp3 audio is captured and updated in the previous DynamoDB record entry.
10. The user tries to list audio with/without postId. (GET)

![walkthrough](media/readme/3.png)

11. A lambda function that handles get requests is triggered and find item(s) in the DynamoDB.
12. The user decides to delete a post with a specific postId. (DELETE)
13. A lambda function will try to remove the mp3 object with the postId.
14. Once the mp3 object is removed from S3, the DynamoDB entry is removed.

## Demo app
* [Here](https://polly-test.edwardskt.com)
