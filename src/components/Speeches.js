import React from 'react';
import { API, Auth } from 'aws-amplify';
import Button from 'material-ui/Button';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import TextField from 'material-ui/TextField';

import AudioControl from './AudioControl';

class Speeches extends React.Component {
  constructor(props) {
    super(props)
    this.state = { speeches: [], postId: '*' }
  }

  listSpeeches = () => {
    this.setState({ speeches: [] });
    return Auth.currentSession().then(session => {
      let postId = '*'
      let options = {
        headers: { Authorization: session.idToken.jwtToken }
      }
      if (this.state.postId.length !== 0) postId = this.state.postId
      return API.get('SpeechRecognitionAPI', '/?postId=' + postId, options)
        .then(response => this.setState({ speeches: response }))
        .catch(error => this.setState({ speeches: [] }))
    })
  }

  updatePostId = e => {
    this.setState({ postId: e.target.value })
  }

  render () {
    return (
      <div>
        <h3>My Speeches</h3>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Post ID</TableCell>
              <TableCell>Voice</TableCell>
              <TableCell>Text</TableCell>
              <TableCell>Status</TableCell>
              <TableCell>Player</TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            { this.state.speeches.map((speech, key) => {
              return (
                <TableRow key={key}>
                  <TableCell>{speech.id}</TableCell>
                  <TableCell>{speech.voice}</TableCell>
                  <TableCell>{speech.text}</TableCell>
                  <TableCell>{speech.status}</TableCell>
                  <TableCell><AudioControl url={speech.url} /></TableCell>
                  <TableCell>Delete</TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
        <Button variant="raised" color="primary" onClick={this.listSpeeches}>Find speeches</Button><TextField label="Post ID" value={this.state.postId} onChange={this.updatePostId} />
      </div>
    )
  };
}

export default Speeches;