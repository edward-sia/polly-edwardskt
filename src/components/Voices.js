import React from 'react';
import Select from 'material-ui/Select';
import { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl } from 'material-ui/Form';

const styles = {
  select: {
    width: '250px',
    marginRight: '10px'
  }
}

class Voices extends React.Component {
  constructor(props) {
    super(props);
    this.state = { voice: '' };
  }

  handleChange = event => {
    this.setState({ voice: event.target.value });
  };

  render() {
    return (
      <FormControl>
        <InputLabel htmlFor="voice-select">Voice</InputLabel>
        <Select
          autoWidth={false}
          value={this.state.voice}
          onChange={this.handleChange}
          style={styles.select}
          inputProps={{
            name: 'voice',
            id: 'voice-select',
          }}
        >
          <MenuItem value="Ivy">Ivy [English - American]</MenuItem>
          {/* <MenuItem value="Joanna">Joanna [English - American]</MenuItem> */}
          <MenuItem value="Joey">Joey [English - American]</MenuItem>
          {/* <MenuItem value="Justin">Justin [English - American]</MenuItem> */}
          {/* <MenuItem value="Kendra">Kendra [English - American]</MenuItem>  */}
          {/* <MenuItem value="Kimberly">Kimberly [English - American]</MenuItem> */}
          {/* <MenuItem value="Salli">Salli [English - American]</MenuItem> */}
          {/* <MenuItem value="Nicole">Nicole [English - Australian]</MenuItem> */}
          <MenuItem value="Russell">Russell [English - Australian]</MenuItem>
          {/* <MenuItem value="Emma">Emma [English - British]</MenuItem> */}
          {/* <MenuItem value="Brian">Brian [English - British]</MenuItem> */}
          <MenuItem value="Amy">Amy [English - British]</MenuItem>
          <MenuItem value="Raveena">Raveena [English - Indian]</MenuItem>        
          {/* <MenuItem value="Geraint">Geraint [English - Welsh]</MenuItem> */}
          {/* <MenuItem value="Ricardo">Ricardo [Brazilian Portuguese]</MenuItem>
          <MenuItem value="Vitória">Vitória [Brazilian Portuguese]</MenuItem>
          <MenuItem value="Lotte">Lotte [Dutch]</MenuItem>
          <MenuItem value="Ruben">Ruben [Dutch]</MenuItem>
          <MenuItem value="Mathieu">Mathieu [French]</MenuItem> */}
          <MenuItem value="Céline">Céline [French]</MenuItem>
          {/* <MenuItem value="Chantal">Chantal [Canadian French]</MenuItem> */}
          <MenuItem value="Marlene">Marlene [German]</MenuItem>
          {/* <MenuItem value="Dóra">Dóra [Icelandic]</MenuItem>
          <MenuItem value="Karl">Karl [Icelandic]</MenuItem>
          <MenuItem value="Carla">Carla [Italian]</MenuItem>
          <MenuItem value="Giorgio">Giorgio [Italian]</MenuItem> */}
          <MenuItem value="Mizuki">Mizuki [Japanese]</MenuItem>
          {/* <MenuItem value="Liv">Liv [Norwegian]</MenuItem>
          <MenuItem value="Maja">Maja [Polish]</MenuItem>
          <MenuItem value="Jan">Jan [Polish]</MenuItem>
          <MenuItem value="Ewa">Ewa [Polish]</MenuItem>
          <MenuItem value="Cristiano">Cristiano [Portuquese]</MenuItem>
          <MenuItem value="Inês">Inês [Portuquese]</MenuItem>
          <MenuItem value="Carmen">Carmen [Romanian]</MenuItem>
          <MenuItem value="Maxim">Maxim [Russian]</MenuItem>
          <MenuItem value="Tatyana">Tatyana [Russian]</MenuItem>
          <MenuItem value="Enrique">Enrique [Spanish]</MenuItem>
          <MenuItem value="Penélope">Penélope [US Spanish]</MenuItem>
          <MenuItem value="Enrique">Miguel [US Spanish]</MenuItem>
          <MenuItem value="Conchita">Conchita [Castilian Spanish]</MenuItem>
          <MenuItem value="Astrid">Astrid [Swedish]</MenuItem>
          <MenuItem value="Filiz">Filiz [Turkish]</MenuItem>
          <MenuItem value="Gwyneth">Gwyneth [Welsh]</MenuItem> */}
        </Select>
      </FormControl>
    )
  }
}

export default Voices;