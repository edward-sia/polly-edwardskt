import React from 'react';
import { API, Auth } from 'aws-amplify';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import Snackbar from 'material-ui/Snackbar';
import { withStyles } from 'material-ui/styles';
import Voices from './Voices';

const styles = theme => ({
  root: theme.mixins.gutters({
    margin: '16px',
    padding: '16px',
    display: 'flex',
    flexDirection: 'column',
  }),
});

class MainControl extends React.Component {
  constructor(props) {
    super(props)
    this.state = { text: '', newPostId: null, loaded: false }
  }

  handleClose = () => {
    this.setState({ loaded: false, newPostId: null });
  }

  onChange = e => {
    this.setState({ text: e.target.value });
  }

  onSubmit = () => {
    if (this.state.text.length === 0) return;
    Auth.currentSession().then(session => {
      const options = {
        headers: {
          Authorization: session.idToken.jwtToken
        },
        body: {
          "voice": "Ivy",
          "text": this.state.text
        }
      }
      return API.post('SpeechRecognitionAPI', '', options)
      .then(response => this.setState({ newPostId: response, loaded: true }))
    })
  }

  render() {
    const { classes } = this.props;
    const { loaded, newPostId } = this.state;
    console.log('====', loaded, newPostId);
      return (
      <Paper className={classes.root} elevation={4}>
        <div style={styles.selectContainer}>
          <Voices />
          <Button variant="raised" color="secondary">Hear my voice</Button>
        </div>
        <TextField
          multiline
          id="convert-text-id"
          label="Text to Speech"
          margin="normal"
          value={this.state.text}
          onChange={this.onChange}
        />
        <Button variant="raised" color="primary" onClick={this.onSubmit}>Convert</Button>
        <Snackbar
          anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
          open={loaded}
          onClose={this.handleClose}
          message={newPostId}
        />
      </Paper>
    )
  }
}

export default withStyles(styles)(MainControl);