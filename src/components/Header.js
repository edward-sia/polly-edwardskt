import React from 'react';
import polly from '../assets/polly.svg';

const Header = () => (
  <header className="App-header">
    <img src={polly} className="App-logo" alt="polly" />
    <h1 className="App-title">Welcome to AWS Polly</h1>
  </header>
)

export default Header;