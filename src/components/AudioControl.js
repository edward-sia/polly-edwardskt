import React from 'react';

const AudioControl = ({ url, controls=true, type='audio/mpeg' }) =>
  <audio controls={controls}><source src={url} type={type} /></audio>

export default AudioControl;
