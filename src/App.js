import React from 'react';
import Amplify from 'aws-amplify';
import { withAuthenticator } from 'aws-amplify-react';
import Header from './components/Header'
import MainControl from './components/MainControl';
import Speeches from './components/Speeches';
import aws_exports from './aws-exports';
import './App.css';

const App = () => (
  <div className="App">
    <Header />
    <MainControl />
    <Speeches />
  </div>
);

Amplify.configure(aws_exports);
export default withAuthenticator(App, { includeGreetings: true });
